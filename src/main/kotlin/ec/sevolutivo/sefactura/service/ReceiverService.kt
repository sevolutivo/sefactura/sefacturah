package ec.sevolutivo.sefactura.service
import ec.sevolutivo.sefactura.domain.Receiver
import ec.sevolutivo.sefactura.repository.ReceiverRepository
import java.util.Optional
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service Implementation for managing [Receiver].
 */
@Service
@Transactional
class ReceiverService(
    private val receiverRepository: ReceiverRepository
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a receiver.
     *
     * @param receiver the entity to save.
     * @return the persisted entity.
     */
    fun save(receiver: Receiver): Receiver {
        log.debug("Request to save Receiver : $receiver")
        return receiverRepository.save(receiver)
    }

    /**
     * Get all the receivers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(pageable: Pageable): Page<Receiver> {
        log.debug("Request to get all Receivers")
        return receiverRepository.findAll(pageable)
    }

    /**
     * Get one receiver by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<Receiver> {
        log.debug("Request to get Receiver : $id")
        return receiverRepository.findById(id)
    }

    /**
     * Delete the receiver by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Receiver : $id")

        receiverRepository.deleteById(id)
    }
}
