package ec.sevolutivo.sefactura.service.dto

import ec.sevolutivo.sefactura.domain.enumeration.IdentificationType
import io.github.jhipster.service.Criteria
import io.github.jhipster.service.filter.Filter
import io.github.jhipster.service.filter.LongFilter
import io.github.jhipster.service.filter.StringFilter
import java.io.Serializable

/**
 * Criteria class for the [ec.sevolutivo.sefactura.domain.Receiver] entity. This class is used in
 * [ec.sevolutivo.sefactura.web.rest.ReceiverResource] to receive all the possible filtering options from the
 * Http GET request parameters.
 * For example the following could be a valid request:
 * ```/receivers?id.greaterThan=5&attr1.contains=something&attr2.specified=false```
 * As Spring is unable to properly convert the types, unless specific [Filter] class are used, we need to use
 * fix type specific filters.
 */
data class ReceiverCriteria(

    var id: LongFilter? = null,

    var name: StringFilter? = null,

    var identificationType: IdentificationTypeFilter? = null,

    var identification: StringFilter? = null,

    var address: StringFilter? = null,

    var phoneNumber: StringFilter? = null,

    var email: StringFilter? = null
) : Serializable, Criteria {

    constructor(other: ReceiverCriteria) :
        this(
            other.id?.copy(),
            other.name?.copy(),
            other.identificationType?.copy(),
            other.identification?.copy(),
            other.address?.copy(),
            other.phoneNumber?.copy(),
            other.email?.copy()
        )

    /**
     * Class for filtering IdentificationType
     */
    class IdentificationTypeFilter : Filter<IdentificationType> {
        constructor()

        constructor(filter: IdentificationTypeFilter) : super(filter)

        override fun copy() = IdentificationTypeFilter(this)
    }

    override fun copy() = ReceiverCriteria(this)

    companion object {
        private const val serialVersionUID: Long = 1L
    }
}
