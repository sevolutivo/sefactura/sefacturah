package ec.sevolutivo.sefactura.service

import ec.sevolutivo.sefactura.domain.Receiver
import ec.sevolutivo.sefactura.domain.Receiver_
import ec.sevolutivo.sefactura.repository.ReceiverRepository
import ec.sevolutivo.sefactura.service.dto.ReceiverCriteria
import io.github.jhipster.service.QueryService
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service for executing complex queries for [Receiver] entities in the database.
 * The main input is a [ReceiverCriteria] which gets converted to [Specification],
 * in a way that all the filters must apply.
 * It returns a [MutableList] of [Receiver] or a [Page] of [Receiver] which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
class ReceiverQueryService(
    private val receiverRepository: ReceiverRepository
) : QueryService<Receiver>() {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Return a [MutableList] of [Receiver] which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    fun findByCriteria(criteria: ReceiverCriteria?): MutableList<Receiver> {
        log.debug("find by criteria : $criteria")
        val specification = createSpecification(criteria)
        return receiverRepository.findAll(specification)
    }

    /**
     * Return a [Page] of [Receiver] which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    fun findByCriteria(criteria: ReceiverCriteria?, page: Pageable): Page<Receiver> {
        log.debug("find by criteria : $criteria, page: $page")
        val specification = createSpecification(criteria)
        return receiverRepository.findAll(specification, page)
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    fun countByCriteria(criteria: ReceiverCriteria?): Long {
        log.debug("count by criteria : $criteria")
        val specification = createSpecification(criteria)
        return receiverRepository.count(specification)
    }

    /**
     * Function to convert [ReceiverCriteria] to a [Specification].
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching [Specification] of the entity.
     */
    protected fun createSpecification(criteria: ReceiverCriteria?): Specification<Receiver?> {
        var specification: Specification<Receiver?> = Specification.where(null)
        if (criteria != null) {
            if (criteria.id != null) {
                specification = specification.and(buildRangeSpecification(criteria.id, Receiver_.id))
            }
            if (criteria.name != null) {
                specification = specification.and(buildStringSpecification(criteria.name, Receiver_.name))
            }
            if (criteria.identificationType != null) {
                specification = specification.and(buildSpecification(criteria.identificationType, Receiver_.identificationType))
            }
            if (criteria.identification != null) {
                specification = specification.and(buildStringSpecification(criteria.identification, Receiver_.identification))
            }
            if (criteria.address != null) {
                specification = specification.and(buildStringSpecification(criteria.address, Receiver_.address))
            }
            if (criteria.phoneNumber != null) {
                specification = specification.and(buildStringSpecification(criteria.phoneNumber, Receiver_.phoneNumber))
            }
            if (criteria.email != null) {
                specification = specification.and(buildStringSpecification(criteria.email, Receiver_.email))
            }
        }
        return specification
    }
}
