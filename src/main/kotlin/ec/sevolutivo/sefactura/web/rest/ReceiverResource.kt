package ec.sevolutivo.sefactura.web.rest

import ec.sevolutivo.sefactura.domain.Receiver
import ec.sevolutivo.sefactura.service.ReceiverQueryService
import ec.sevolutivo.sefactura.service.ReceiverService
import ec.sevolutivo.sefactura.service.dto.ReceiverCriteria
import ec.sevolutivo.sefactura.web.rest.errors.BadRequestAlertException
import io.github.jhipster.web.util.HeaderUtil
import io.github.jhipster.web.util.PaginationUtil
import io.github.jhipster.web.util.ResponseUtil
import java.net.URI
import java.net.URISyntaxException
import javax.validation.Valid
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

private const val ENTITY_NAME = "receiver"
/**
 * REST controller for managing [ec.sevolutivo.sefactura.domain.Receiver].
 */
@RestController
@RequestMapping("/api")
class ReceiverResource(
    private val receiverService: ReceiverService,
    private val receiverQueryService: ReceiverQueryService
) {

    private val log = LoggerFactory.getLogger(javaClass)
    @Value("\${jhipster.clientApp.name}")
    private var applicationName: String? = null

    /**
     * `POST  /receivers` : Create a new receiver.
     *
     * @param receiver the receiver to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new receiver, or with status `400 (Bad Request)` if the receiver has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/receivers")
    fun createReceiver(@Valid @RequestBody receiver: Receiver): ResponseEntity<Receiver> {
        log.debug("REST request to save Receiver : $receiver")
        if (receiver.id != null) {
            throw BadRequestAlertException(
                "A new receiver cannot already have an ID",
                ENTITY_NAME, "idexists"
            )
        }
        val result = receiverService.save(receiver)
        return ResponseEntity.created(URI("/api/receivers/${result.id}"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.id.toString()))
            .body(result)
    }

    /**
     * `PUT  /receivers` : Updates an existing receiver.
     *
     * @param receiver the receiver to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated receiver,
     * or with status `400 (Bad Request)` if the receiver is not valid,
     * or with status `500 (Internal Server Error)` if the receiver couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/receivers")
    fun updateReceiver(@Valid @RequestBody receiver: Receiver): ResponseEntity<Receiver> {
        log.debug("REST request to update Receiver : $receiver")
        if (receiver.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        val result = receiverService.save(receiver)
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName, false, ENTITY_NAME,
                     receiver.id.toString()
                )
            )
            .body(result)
    }
    /**
     * `GET  /receivers` : get all the receivers.
     *
     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the [ResponseEntity] with status `200 (OK)` and the list of receivers in body.
     */
    @GetMapping("/receivers") fun getAllReceivers(
        criteria: ReceiverCriteria,
        pageable: Pageable

    ): ResponseEntity<MutableList<Receiver>> {
        log.debug("REST request to get Receivers by criteria: $criteria")
        val page = receiverQueryService.findByCriteria(criteria, pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
        return ResponseEntity.ok().headers(headers).body(page.content)
    }

    /**
     * `GET  /receivers/count}` : count all the receivers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the [ResponseEntity] with status `200 (OK)` and the count in body.
     */
    @GetMapping("/receivers/count")
    fun countReceivers(criteria: ReceiverCriteria): ResponseEntity<Long> {
        log.debug("REST request to count Receivers by criteria: $criteria")
        return ResponseEntity.ok().body(receiverQueryService.countByCriteria(criteria))
    }

    /**
     * `GET  /receivers/:id` : get the "id" receiver.
     *
     * @param id the id of the receiver to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the receiver, or with status `404 (Not Found)`.
     */
    @GetMapping("/receivers/{id}")
    fun getReceiver(@PathVariable id: Long): ResponseEntity<Receiver> {
        log.debug("REST request to get Receiver : $id")
        val receiver = receiverService.findOne(id)
        return ResponseUtil.wrapOrNotFound(receiver)
    }
    /**
     *  `DELETE  /receivers/:id` : delete the "id" receiver.
     *
     * @param id the id of the receiver to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/receivers/{id}")
    fun deleteReceiver(@PathVariable id: Long): ResponseEntity<Void> {
        log.debug("REST request to delete Receiver : $id")

        receiverService.delete(id)
            return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build()
    }
}
