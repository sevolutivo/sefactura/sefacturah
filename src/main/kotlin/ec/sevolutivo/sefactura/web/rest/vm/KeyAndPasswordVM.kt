package ec.sevolutivo.sefactura.web.rest.vm

/**
 * View Model object for storing the user's key and password.
 */
class KeyAndPasswordVM(var key: String? = null, var newPassword: String? = null)
