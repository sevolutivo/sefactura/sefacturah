package ec.sevolutivo.sefactura.repository

import ec.sevolutivo.sefactura.domain.Receiver
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Receiver] entity.
 */
@Suppress("unused")
@Repository
interface ReceiverRepository : JpaRepository<Receiver, Long>, JpaSpecificationExecutor<Receiver>
