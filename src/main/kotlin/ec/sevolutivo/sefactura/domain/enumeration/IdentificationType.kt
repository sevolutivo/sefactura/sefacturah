package ec.sevolutivo.sefactura.domain.enumeration

/**
 * The IdentificationType enumeration.
 */
enum class IdentificationType {
        CI, RUC, PASSPORT
}
