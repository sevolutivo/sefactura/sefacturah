package ec.sevolutivo.sefactura.domain

import ec.sevolutivo.sefactura.domain.enumeration.IdentificationType
import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.*
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy

/**
 * A Receiver.
 */
@Entity
@Table(name = "receiver")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Receiver(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @get: NotNull
    @get: Size(max = 100)
    @Column(name = "name", length = 100, nullable = false)
    var name: String? = null,

    @get: NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "identification_type", nullable = false)
    var identificationType: IdentificationType? = null,

    @get: NotNull
    @get: Size(max = 13)
    @Column(name = "identification", length = 13, nullable = false)
    var identification: String? = null,

    @get: NotNull
    @Column(name = "address", nullable = false)
    var address: String? = null,

    @get: Size(max = 10)
    @Column(name = "phone_number", length = 10)
    var phoneNumber: String? = null,

    @get: NotNull
    @get: Size(max = 64)
    @Column(name = "email", length = 64, nullable = false)
    var email: String? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here
) : Serializable {
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Receiver) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Receiver{" +
        "id=$id" +
        ", name='$name'" +
        ", identificationType='$identificationType'" +
        ", identification='$identification'" +
        ", address='$address'" +
        ", phoneNumber='$phoneNumber'" +
        ", email='$email'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
