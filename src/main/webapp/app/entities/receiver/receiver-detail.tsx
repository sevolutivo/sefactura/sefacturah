import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './receiver.reducer';
import { IReceiver } from 'app/shared/model/receiver.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IReceiverDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ReceiverDetail = (props: IReceiverDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { receiverEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Receiver [<b>{receiverEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{receiverEntity.name}</dd>
          <dt>
            <span id="identificationType">Identification Type</span>
          </dt>
          <dd>{receiverEntity.identificationType}</dd>
          <dt>
            <span id="identification">Identification</span>
          </dt>
          <dd>{receiverEntity.identification}</dd>
          <dt>
            <span id="address">Address</span>
          </dt>
          <dd>{receiverEntity.address}</dd>
          <dt>
            <span id="phoneNumber">Phone Number</span>
          </dt>
          <dd>{receiverEntity.phoneNumber}</dd>
          <dt>
            <span id="email">Email</span>
          </dt>
          <dd>{receiverEntity.email}</dd>
        </dl>
        <Button tag={Link} to="/receiver" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/receiver/${receiverEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ receiver }: IRootState) => ({
  receiverEntity: receiver.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ReceiverDetail);
