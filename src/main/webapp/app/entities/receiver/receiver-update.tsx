import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './receiver.reducer';
import { IReceiver } from 'app/shared/model/receiver.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IReceiverUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ReceiverUpdate = (props: IReceiverUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { receiverEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/receiver' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...receiverEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="sefacturaApp.receiver.home.createOrEditLabel">Create or edit a Receiver</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : receiverEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="receiver-id">ID</Label>
                  <AvInput id="receiver-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="receiver-name">
                  Name
                </Label>
                <AvField
                  id="receiver-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    maxLength: { value: 100, errorMessage: 'This field cannot be longer than 100 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="identificationTypeLabel" for="receiver-identificationType">
                  Identification Type
                </Label>
                <AvInput
                  id="receiver-identificationType"
                  type="select"
                  className="form-control"
                  name="identificationType"
                  value={(!isNew && receiverEntity.identificationType) || 'CI'}
                >
                  <option value="CI">CI</option>
                  <option value="RUC">RUC</option>
                  <option value="PASSPORT">PASSPORT</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="identificationLabel" for="receiver-identification">
                  Identification
                </Label>
                <AvField
                  id="receiver-identification"
                  type="text"
                  name="identification"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    maxLength: { value: 13, errorMessage: 'This field cannot be longer than 13 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="addressLabel" for="receiver-address">
                  Address
                </Label>
                <AvField
                  id="receiver-address"
                  type="text"
                  name="address"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="phoneNumberLabel" for="receiver-phoneNumber">
                  Phone Number
                </Label>
                <AvField
                  id="receiver-phoneNumber"
                  type="text"
                  name="phoneNumber"
                  validate={{
                    maxLength: { value: 10, errorMessage: 'This field cannot be longer than 10 characters.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="emailLabel" for="receiver-email">
                  Email
                </Label>
                <AvField
                  id="receiver-email"
                  type="text"
                  name="email"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    maxLength: { value: 64, errorMessage: 'This field cannot be longer than 64 characters.' },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/receiver" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  receiverEntity: storeState.receiver.entity,
  loading: storeState.receiver.loading,
  updating: storeState.receiver.updating,
  updateSuccess: storeState.receiver.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ReceiverUpdate);
