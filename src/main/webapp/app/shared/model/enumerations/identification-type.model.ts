export const enum IdentificationType {
  CI = 'CI',

  RUC = 'RUC',

  PASSPORT = 'PASSPORT',
}
