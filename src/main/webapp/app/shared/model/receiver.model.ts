import { IdentificationType } from 'app/shared/model/enumerations/identification-type.model';

export interface IReceiver {
  id?: number;
  name?: string;
  identificationType?: IdentificationType;
  identification?: string;
  address?: string;
  phoneNumber?: string;
  email?: string;
}

export const defaultValue: Readonly<IReceiver> = {};
