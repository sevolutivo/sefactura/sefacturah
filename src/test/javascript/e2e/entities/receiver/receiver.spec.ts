import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ReceiverComponentsPage from './receiver.page-object';
import ReceiverUpdatePage from './receiver-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Receiver e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let receiverComponentsPage: ReceiverComponentsPage;
  let receiverUpdatePage: ReceiverUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    receiverComponentsPage = new ReceiverComponentsPage();
    receiverComponentsPage = await receiverComponentsPage.goToPage(navBarPage);
  });

  it('should load Receivers', async () => {
    expect(await receiverComponentsPage.title.getText()).to.match(/Receivers/);
    expect(await receiverComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Receivers', async () => {
    const beforeRecordsCount = (await isVisible(receiverComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(receiverComponentsPage.table);
    receiverUpdatePage = await receiverComponentsPage.goToCreateReceiver();
    await receiverUpdatePage.enterData();

    expect(await receiverComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(receiverComponentsPage.table);
    await waitUntilCount(receiverComponentsPage.records, beforeRecordsCount + 1);
    expect(await receiverComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await receiverComponentsPage.deleteReceiver();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(receiverComponentsPage.records, beforeRecordsCount);
      expect(await receiverComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(receiverComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
