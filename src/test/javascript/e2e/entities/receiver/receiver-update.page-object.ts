import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ReceiverUpdatePage {
  pageTitle: ElementFinder = element(by.id('sefacturaApp.receiver.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#receiver-name'));
  identificationTypeSelect: ElementFinder = element(by.css('select#receiver-identificationType'));
  identificationInput: ElementFinder = element(by.css('input#receiver-identification'));
  addressInput: ElementFinder = element(by.css('input#receiver-address'));
  phoneNumberInput: ElementFinder = element(by.css('input#receiver-phoneNumber'));
  emailInput: ElementFinder = element(by.css('input#receiver-email'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setIdentificationTypeSelect(identificationType) {
    await this.identificationTypeSelect.sendKeys(identificationType);
  }

  async getIdentificationTypeSelect() {
    return this.identificationTypeSelect.element(by.css('option:checked')).getText();
  }

  async identificationTypeSelectLastOption() {
    await this.identificationTypeSelect.all(by.tagName('option')).last().click();
  }
  async setIdentificationInput(identification) {
    await this.identificationInput.sendKeys(identification);
  }

  async getIdentificationInput() {
    return this.identificationInput.getAttribute('value');
  }

  async setAddressInput(address) {
    await this.addressInput.sendKeys(address);
  }

  async getAddressInput() {
    return this.addressInput.getAttribute('value');
  }

  async setPhoneNumberInput(phoneNumber) {
    await this.phoneNumberInput.sendKeys(phoneNumber);
  }

  async getPhoneNumberInput() {
    return this.phoneNumberInput.getAttribute('value');
  }

  async setEmailInput(email) {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput() {
    return this.emailInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setNameInput('name');
    expect(await this.getNameInput()).to.match(/name/);
    await waitUntilDisplayed(this.saveButton);
    await this.identificationTypeSelectLastOption();
    await waitUntilDisplayed(this.saveButton);
    await this.setIdentificationInput('1712345678001');
    expect(await this.getIdentificationInput()).to.match(/1712345678001/);
    await waitUntilDisplayed(this.saveButton);
    await this.setAddressInput('address');
    expect(await this.getAddressInput()).to.match(/address/);
    await waitUntilDisplayed(this.saveButton);
    await this.setPhoneNumberInput('555-1234');
    expect(await this.getPhoneNumberInput()).to.match(/555-1234/);
    await waitUntilDisplayed(this.saveButton);
    await this.setEmailInput('email');
    expect(await this.getEmailInput()).to.match(/email/);
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
