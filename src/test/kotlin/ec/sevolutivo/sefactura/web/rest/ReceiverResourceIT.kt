package ec.sevolutivo.sefactura.web.rest

import ec.sevolutivo.sefactura.SefacturaApp
import ec.sevolutivo.sefactura.domain.Receiver
import ec.sevolutivo.sefactura.domain.enumeration.IdentificationType
import ec.sevolutivo.sefactura.repository.ReceiverRepository
import ec.sevolutivo.sefactura.service.ReceiverQueryService
import ec.sevolutivo.sefactura.service.ReceiverService
import ec.sevolutivo.sefactura.web.rest.errors.ExceptionTranslator
import javax.persistence.EntityManager
import kotlin.test.assertNotNull
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator

/**
 * Integration tests for the [ReceiverResource] REST controller.
 *
 * @see ReceiverResource
 */
@SpringBootTest(classes = [SefacturaApp::class])
@AutoConfigureMockMvc
@WithMockUser
class ReceiverResourceIT {

    @Autowired
    private lateinit var receiverRepository: ReceiverRepository

    @Autowired
    private lateinit var receiverService: ReceiverService

    @Autowired
    private lateinit var receiverQueryService: ReceiverQueryService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restReceiverMockMvc: MockMvc

    private lateinit var receiver: Receiver

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val receiverResource = ReceiverResource(receiverService, receiverQueryService)
         this.restReceiverMockMvc = MockMvcBuilders.standaloneSetup(receiverResource)
             .setCustomArgumentResolvers(pageableArgumentResolver)
             .setControllerAdvice(exceptionTranslator)
             .setConversionService(createFormattingConversionService())
             .setMessageConverters(jacksonMessageConverter)
             .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        receiver = createEntity(em)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun createReceiver() {
        val databaseSizeBeforeCreate = receiverRepository.findAll().size

        // Create the Receiver
        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isCreated)

        // Validate the Receiver in the database
        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeCreate + 1)
        val testReceiver = receiverList[receiverList.size - 1]
        assertThat(testReceiver.name).isEqualTo(DEFAULT_NAME)
        assertThat(testReceiver.identificationType).isEqualTo(DEFAULT_IDENTIFICATION_TYPE)
        assertThat(testReceiver.identification).isEqualTo(DEFAULT_IDENTIFICATION)
        assertThat(testReceiver.address).isEqualTo(DEFAULT_ADDRESS)
        assertThat(testReceiver.phoneNumber).isEqualTo(DEFAULT_PHONE_NUMBER)
        assertThat(testReceiver.email).isEqualTo(DEFAULT_EMAIL)
    }

    @Test
    @Transactional
    fun createReceiverWithExistingId() {
        val databaseSizeBeforeCreate = receiverRepository.findAll().size

        // Create the Receiver with an existing ID
        receiver.id = 1L

        // An entity with an existing ID cannot be created, so this API call must fail
        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        // Validate the Receiver in the database
        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeCreate)
    }

    @Test
    @Transactional
    fun checkNameIsRequired() {
        val databaseSizeBeforeTest = receiverRepository.findAll().size
        // set the field null
        receiver.name = null

        // Create the Receiver, which fails.

        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    fun checkIdentificationTypeIsRequired() {
        val databaseSizeBeforeTest = receiverRepository.findAll().size
        // set the field null
        receiver.identificationType = null

        // Create the Receiver, which fails.

        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    fun checkIdentificationIsRequired() {
        val databaseSizeBeforeTest = receiverRepository.findAll().size
        // set the field null
        receiver.identification = null

        // Create the Receiver, which fails.

        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    fun checkAddressIsRequired() {
        val databaseSizeBeforeTest = receiverRepository.findAll().size
        // set the field null
        receiver.address = null

        // Create the Receiver, which fails.

        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    fun checkEmailIsRequired() {
        val databaseSizeBeforeTest = receiverRepository.findAll().size
        // set the field null
        receiver.email = null

        // Create the Receiver, which fails.

        restReceiverMockMvc.perform(
            post("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeTest)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceivers() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList
        restReceiverMockMvc.perform(get("/api/receivers?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(receiver.id?.toInt())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].identificationType").value(hasItem(DEFAULT_IDENTIFICATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].identification").value(hasItem(DEFAULT_IDENTIFICATION)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL))) }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getReceiver() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        val id = receiver.id
        assertNotNull(id)

        // Get the receiver
        restReceiverMockMvc.perform(get("/api/receivers/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(receiver.id?.toInt()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.identificationType").value(DEFAULT_IDENTIFICATION_TYPE.toString()))
            .andExpect(jsonPath("$.identification").value(DEFAULT_IDENTIFICATION))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL)) }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getReceiversByIdFiltering() {
      // Initialize the database
      receiverRepository.saveAndFlush(receiver)
      val id = receiver.id

      defaultReceiverShouldBeFound("id.equals=" + id)
      defaultReceiverShouldNotBeFound("id.notEquals=" + id)

      defaultReceiverShouldBeFound("id.greaterThanOrEqual=" + id)
      defaultReceiverShouldNotBeFound("id.greaterThan=" + id)

      defaultReceiverShouldBeFound("id.lessThanOrEqual=" + id)
      defaultReceiverShouldNotBeFound("id.lessThan=" + id)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByNameIsEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where name equals to DEFAULT_NAME
        defaultReceiverShouldBeFound("name.equals=$DEFAULT_NAME")

        // Get all the receiverList where name equals to UPDATED_NAME
        defaultReceiverShouldNotBeFound("name.equals=$UPDATED_NAME")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByNameIsNotEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where name not equals to DEFAULT_NAME
        defaultReceiverShouldNotBeFound("name.notEquals=" + DEFAULT_NAME)

        // Get all the receiverList where name not equals to UPDATED_NAME
        defaultReceiverShouldBeFound("name.notEquals=" + UPDATED_NAME)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByNameIsInShouldWork() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where name in DEFAULT_NAME or UPDATED_NAME
        defaultReceiverShouldBeFound("name.in=$DEFAULT_NAME,$UPDATED_NAME")

        // Get all the receiverList where name equals to UPDATED_NAME
        defaultReceiverShouldNotBeFound("name.in=$UPDATED_NAME")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByNameIsNullOrNotNull() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where name is not null
        defaultReceiverShouldBeFound("name.specified=true")

        // Get all the receiverList where name is null
        defaultReceiverShouldNotBeFound("name.specified=false")
    }
    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByNameContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where name contains DEFAULT_NAME
        defaultReceiverShouldBeFound("name.contains=" + DEFAULT_NAME)

        // Get all the receiverList where name contains UPDATED_NAME
        defaultReceiverShouldNotBeFound("name.contains=" + UPDATED_NAME)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByNameNotContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where name does not contain DEFAULT_NAME
        defaultReceiverShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME)

        // Get all the receiverList where name does not contain UPDATED_NAME
        defaultReceiverShouldBeFound("name.doesNotContain=" + UPDATED_NAME)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationTypeIsEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identificationType equals to DEFAULT_IDENTIFICATION_TYPE
        defaultReceiverShouldBeFound("identificationType.equals=$DEFAULT_IDENTIFICATION_TYPE")

        // Get all the receiverList where identificationType equals to UPDATED_IDENTIFICATION_TYPE
        defaultReceiverShouldNotBeFound("identificationType.equals=$UPDATED_IDENTIFICATION_TYPE")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationTypeIsNotEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identificationType not equals to DEFAULT_IDENTIFICATION_TYPE
        defaultReceiverShouldNotBeFound("identificationType.notEquals=" + DEFAULT_IDENTIFICATION_TYPE)

        // Get all the receiverList where identificationType not equals to UPDATED_IDENTIFICATION_TYPE
        defaultReceiverShouldBeFound("identificationType.notEquals=" + UPDATED_IDENTIFICATION_TYPE)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationTypeIsInShouldWork() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identificationType in DEFAULT_IDENTIFICATION_TYPE or UPDATED_IDENTIFICATION_TYPE
        defaultReceiverShouldBeFound("identificationType.in=$DEFAULT_IDENTIFICATION_TYPE,$UPDATED_IDENTIFICATION_TYPE")

        // Get all the receiverList where identificationType equals to UPDATED_IDENTIFICATION_TYPE
        defaultReceiverShouldNotBeFound("identificationType.in=$UPDATED_IDENTIFICATION_TYPE")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationTypeIsNullOrNotNull() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identificationType is not null
        defaultReceiverShouldBeFound("identificationType.specified=true")

        // Get all the receiverList where identificationType is null
        defaultReceiverShouldNotBeFound("identificationType.specified=false")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationIsEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identification equals to DEFAULT_IDENTIFICATION
        defaultReceiverShouldBeFound("identification.equals=$DEFAULT_IDENTIFICATION")

        // Get all the receiverList where identification equals to UPDATED_IDENTIFICATION
        defaultReceiverShouldNotBeFound("identification.equals=$UPDATED_IDENTIFICATION")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationIsNotEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identification not equals to DEFAULT_IDENTIFICATION
        defaultReceiverShouldNotBeFound("identification.notEquals=" + DEFAULT_IDENTIFICATION)

        // Get all the receiverList where identification not equals to UPDATED_IDENTIFICATION
        defaultReceiverShouldBeFound("identification.notEquals=" + UPDATED_IDENTIFICATION)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationIsInShouldWork() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identification in DEFAULT_IDENTIFICATION or UPDATED_IDENTIFICATION
        defaultReceiverShouldBeFound("identification.in=$DEFAULT_IDENTIFICATION,$UPDATED_IDENTIFICATION")

        // Get all the receiverList where identification equals to UPDATED_IDENTIFICATION
        defaultReceiverShouldNotBeFound("identification.in=$UPDATED_IDENTIFICATION")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationIsNullOrNotNull() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identification is not null
        defaultReceiverShouldBeFound("identification.specified=true")

        // Get all the receiverList where identification is null
        defaultReceiverShouldNotBeFound("identification.specified=false")
    }
    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identification contains DEFAULT_IDENTIFICATION
        defaultReceiverShouldBeFound("identification.contains=" + DEFAULT_IDENTIFICATION)

        // Get all the receiverList where identification contains UPDATED_IDENTIFICATION
        defaultReceiverShouldNotBeFound("identification.contains=" + UPDATED_IDENTIFICATION)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByIdentificationNotContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where identification does not contain DEFAULT_IDENTIFICATION
        defaultReceiverShouldNotBeFound("identification.doesNotContain=" + DEFAULT_IDENTIFICATION)

        // Get all the receiverList where identification does not contain UPDATED_IDENTIFICATION
        defaultReceiverShouldBeFound("identification.doesNotContain=" + UPDATED_IDENTIFICATION)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByAddressIsEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where address equals to DEFAULT_ADDRESS
        defaultReceiverShouldBeFound("address.equals=$DEFAULT_ADDRESS")

        // Get all the receiverList where address equals to UPDATED_ADDRESS
        defaultReceiverShouldNotBeFound("address.equals=$UPDATED_ADDRESS")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByAddressIsNotEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where address not equals to DEFAULT_ADDRESS
        defaultReceiverShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS)

        // Get all the receiverList where address not equals to UPDATED_ADDRESS
        defaultReceiverShouldBeFound("address.notEquals=" + UPDATED_ADDRESS)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByAddressIsInShouldWork() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultReceiverShouldBeFound("address.in=$DEFAULT_ADDRESS,$UPDATED_ADDRESS")

        // Get all the receiverList where address equals to UPDATED_ADDRESS
        defaultReceiverShouldNotBeFound("address.in=$UPDATED_ADDRESS")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByAddressIsNullOrNotNull() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where address is not null
        defaultReceiverShouldBeFound("address.specified=true")

        // Get all the receiverList where address is null
        defaultReceiverShouldNotBeFound("address.specified=false")
    }
    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByAddressContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where address contains DEFAULT_ADDRESS
        defaultReceiverShouldBeFound("address.contains=" + DEFAULT_ADDRESS)

        // Get all the receiverList where address contains UPDATED_ADDRESS
        defaultReceiverShouldNotBeFound("address.contains=" + UPDATED_ADDRESS)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByAddressNotContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where address does not contain DEFAULT_ADDRESS
        defaultReceiverShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS)

        // Get all the receiverList where address does not contain UPDATED_ADDRESS
        defaultReceiverShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByPhoneNumberIsEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where phoneNumber equals to DEFAULT_PHONE_NUMBER
        defaultReceiverShouldBeFound("phoneNumber.equals=$DEFAULT_PHONE_NUMBER")

        // Get all the receiverList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultReceiverShouldNotBeFound("phoneNumber.equals=$UPDATED_PHONE_NUMBER")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByPhoneNumberIsNotEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where phoneNumber not equals to DEFAULT_PHONE_NUMBER
        defaultReceiverShouldNotBeFound("phoneNumber.notEquals=" + DEFAULT_PHONE_NUMBER)

        // Get all the receiverList where phoneNumber not equals to UPDATED_PHONE_NUMBER
        defaultReceiverShouldBeFound("phoneNumber.notEquals=" + UPDATED_PHONE_NUMBER)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByPhoneNumberIsInShouldWork() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where phoneNumber in DEFAULT_PHONE_NUMBER or UPDATED_PHONE_NUMBER
        defaultReceiverShouldBeFound("phoneNumber.in=$DEFAULT_PHONE_NUMBER,$UPDATED_PHONE_NUMBER")

        // Get all the receiverList where phoneNumber equals to UPDATED_PHONE_NUMBER
        defaultReceiverShouldNotBeFound("phoneNumber.in=$UPDATED_PHONE_NUMBER")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByPhoneNumberIsNullOrNotNull() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where phoneNumber is not null
        defaultReceiverShouldBeFound("phoneNumber.specified=true")

        // Get all the receiverList where phoneNumber is null
        defaultReceiverShouldNotBeFound("phoneNumber.specified=false")
    }
    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByPhoneNumberContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where phoneNumber contains DEFAULT_PHONE_NUMBER
        defaultReceiverShouldBeFound("phoneNumber.contains=" + DEFAULT_PHONE_NUMBER)

        // Get all the receiverList where phoneNumber contains UPDATED_PHONE_NUMBER
        defaultReceiverShouldNotBeFound("phoneNumber.contains=" + UPDATED_PHONE_NUMBER)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByPhoneNumberNotContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where phoneNumber does not contain DEFAULT_PHONE_NUMBER
        defaultReceiverShouldNotBeFound("phoneNumber.doesNotContain=" + DEFAULT_PHONE_NUMBER)

        // Get all the receiverList where phoneNumber does not contain UPDATED_PHONE_NUMBER
        defaultReceiverShouldBeFound("phoneNumber.doesNotContain=" + UPDATED_PHONE_NUMBER)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByEmailIsEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where email equals to DEFAULT_EMAIL
        defaultReceiverShouldBeFound("email.equals=$DEFAULT_EMAIL")

        // Get all the receiverList where email equals to UPDATED_EMAIL
        defaultReceiverShouldNotBeFound("email.equals=$UPDATED_EMAIL")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByEmailIsNotEqualToSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where email not equals to DEFAULT_EMAIL
        defaultReceiverShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL)

        // Get all the receiverList where email not equals to UPDATED_EMAIL
        defaultReceiverShouldBeFound("email.notEquals=" + UPDATED_EMAIL)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByEmailIsInShouldWork() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultReceiverShouldBeFound("email.in=$DEFAULT_EMAIL,$UPDATED_EMAIL")

        // Get all the receiverList where email equals to UPDATED_EMAIL
        defaultReceiverShouldNotBeFound("email.in=$UPDATED_EMAIL")
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByEmailIsNullOrNotNull() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where email is not null
        defaultReceiverShouldBeFound("email.specified=true")

        // Get all the receiverList where email is null
        defaultReceiverShouldNotBeFound("email.specified=false")
    }
    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByEmailContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where email contains DEFAULT_EMAIL
        defaultReceiverShouldBeFound("email.contains=" + DEFAULT_EMAIL)

        // Get all the receiverList where email contains UPDATED_EMAIL
        defaultReceiverShouldNotBeFound("email.contains=" + UPDATED_EMAIL)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllReceiversByEmailNotContainsSomething() {
        // Initialize the database
        receiverRepository.saveAndFlush(receiver)

        // Get all the receiverList where email does not contain DEFAULT_EMAIL
        defaultReceiverShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL)

        // Get all the receiverList where email does not contain UPDATED_EMAIL
        defaultReceiverShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL)
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    @Throws(Exception::class)
    private fun defaultReceiverShouldBeFound(filter: String) {
        restReceiverMockMvc.perform(get("/api/receivers?sort=id,desc&$filter"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(receiver.id?.toInt())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].identificationType").value(hasItem(DEFAULT_IDENTIFICATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].identification").value(hasItem(DEFAULT_IDENTIFICATION)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))

        // Check, that the count call also returns 1
        restReceiverMockMvc.perform(get("/api/receivers/count?sort=id,desc&$filter"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"))
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    @Throws(Exception::class)
    private fun defaultReceiverShouldNotBeFound(filter: String) {
        restReceiverMockMvc.perform(get("/api/receivers?sort=id,desc&$filter"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray)
            .andExpect(jsonPath("$").isEmpty)

        // Check, that the count call also returns 0
        restReceiverMockMvc.perform(get("/api/receivers/count?sort=id,desc&$filter"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingReceiver() {
        // Get the receiver
        restReceiverMockMvc.perform(get("/api/receivers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }
    @Test
    @Transactional
    fun updateReceiver() {
        // Initialize the database
        receiverService.save(receiver)

        val databaseSizeBeforeUpdate = receiverRepository.findAll().size

        // Update the receiver
        val id = receiver.id
        assertNotNull(id)
        val updatedReceiver = receiverRepository.findById(id).get()
        // Disconnect from session so that the updates on updatedReceiver are not directly saved in db
        em.detach(updatedReceiver)
        updatedReceiver.name = UPDATED_NAME
        updatedReceiver.identificationType = UPDATED_IDENTIFICATION_TYPE
        updatedReceiver.identification = UPDATED_IDENTIFICATION
        updatedReceiver.address = UPDATED_ADDRESS
        updatedReceiver.phoneNumber = UPDATED_PHONE_NUMBER
        updatedReceiver.email = UPDATED_EMAIL

        restReceiverMockMvc.perform(
            put("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(updatedReceiver))
        ).andExpect(status().isOk)

        // Validate the Receiver in the database
        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeUpdate)
        val testReceiver = receiverList[receiverList.size - 1]
        assertThat(testReceiver.name).isEqualTo(UPDATED_NAME)
        assertThat(testReceiver.identificationType).isEqualTo(UPDATED_IDENTIFICATION_TYPE)
        assertThat(testReceiver.identification).isEqualTo(UPDATED_IDENTIFICATION)
        assertThat(testReceiver.address).isEqualTo(UPDATED_ADDRESS)
        assertThat(testReceiver.phoneNumber).isEqualTo(UPDATED_PHONE_NUMBER)
        assertThat(testReceiver.email).isEqualTo(UPDATED_EMAIL)
    }

    @Test
    @Transactional
    fun updateNonExistingReceiver() {
        val databaseSizeBeforeUpdate = receiverRepository.findAll().size

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReceiverMockMvc.perform(
            put("/api/receivers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(receiver))
        ).andExpect(status().isBadRequest)

        // Validate the Receiver in the database
        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeUpdate)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun deleteReceiver() {
        // Initialize the database
        receiverService.save(receiver)

        val databaseSizeBeforeDelete = receiverRepository.findAll().size

        // Delete the receiver
        restReceiverMockMvc.perform(
            delete("/api/receivers/{id}", receiver.id)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent)

        // Validate the database contains one less item
        val receiverList = receiverRepository.findAll()
        assertThat(receiverList).hasSize(databaseSizeBeforeDelete - 1)
    }

    companion object {

        private const val DEFAULT_NAME = "AAAAAAAAAA"
        private const val UPDATED_NAME = "BBBBBBBBBB"

        private val DEFAULT_IDENTIFICATION_TYPE: IdentificationType = IdentificationType.CI
        private val UPDATED_IDENTIFICATION_TYPE: IdentificationType = IdentificationType.RUC

        private const val DEFAULT_IDENTIFICATION = "AAAAAAAAAA"
        private const val UPDATED_IDENTIFICATION = "BBBBBBBBBB"

        private const val DEFAULT_ADDRESS = "AAAAAAAAAA"
        private const val UPDATED_ADDRESS = "BBBBBBBBBB"

        private const val DEFAULT_PHONE_NUMBER = "AAAAAAAAAA"
        private const val UPDATED_PHONE_NUMBER = "BBBBBBBBBB"

        private const val DEFAULT_EMAIL = "AAAAAAAAAA"
        private const val UPDATED_EMAIL = "BBBBBBBBBB"

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Receiver {
            val receiver = Receiver(
                name = DEFAULT_NAME,
                identificationType = DEFAULT_IDENTIFICATION_TYPE,
                identification = DEFAULT_IDENTIFICATION,
                address = DEFAULT_ADDRESS,
                phoneNumber = DEFAULT_PHONE_NUMBER,
                email = DEFAULT_EMAIL
            )

            return receiver
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Receiver {
            val receiver = Receiver(
                name = UPDATED_NAME,
                identificationType = UPDATED_IDENTIFICATION_TYPE,
                identification = UPDATED_IDENTIFICATION,
                address = UPDATED_ADDRESS,
                phoneNumber = UPDATED_PHONE_NUMBER,
                email = UPDATED_EMAIL
            )

            return receiver
        }
    }
}
