package ec.sevolutivo.sefactura

import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import org.junit.jupiter.api.Test

class ArchTest {

    @Test
    fun servicesAndRepositoriesShouldNotDependOnWebLayer() {

        val importedClasses = ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("ec.sevolutivo.sefactura")

        noClasses()
            .that()
                .resideInAnyPackage("ec.sevolutivo.sefactura.service..")
            .or()
                .resideInAnyPackage("ec.sevolutivo.sefactura.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..ec.sevolutivo.sefactura.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses)
    }
}
