package ec.sevolutivo.sefactura.domain

import ec.sevolutivo.sefactura.web.rest.equalsVerifier
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ReceiverTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Receiver::class)
        val receiver1 = Receiver()
        receiver1.id = 1L
        val receiver2 = Receiver()
        receiver2.id = receiver1.id
        assertThat(receiver1).isEqualTo(receiver2)
        receiver2.id = 2L
        assertThat(receiver1).isNotEqualTo(receiver2)
        receiver1.id = null
        assertThat(receiver1).isNotEqualTo(receiver2)
    }
}
